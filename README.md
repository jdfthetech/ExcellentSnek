# ExcellentSnek
Sorts excel data from an import file, manipulates the data in memory and spits out a new file with your sorted data using Python3, and OpenPyxl

You will need OpenPyxl for this python script to work (see venv documentation if you would like to skip this)

This is bare bones code left here for anyone looking to do some quick and dirty data manipulation.

The python script is written for linux, you may remove the #! /usr/bin/python3 if using a windows machine

The included IMPORTDATA.xlsx file is simply to help you test.

IMPORTANT VIRTUAL ENVIRONMENT (VENV) DOCUMENTATION

The venv directory should include all files you will need (as long as you have python3).  You can clone this and use this command to access the virtual environment: 

source /path/you/put/folder/venv/bin/activate

you can use this command to leave the virtual environment:

deactivate

When in the virtual environment, you can execute the python script and it will use the included packages without the need for you to install them on your system.


